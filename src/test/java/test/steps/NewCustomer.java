package test.steps;

import java.util.Map;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test.utils.ByUtils;
import test.utils.Varriable;

public class NewCustomer extends BasePageAndroid {
	@When("^i navigate to new customer page$")
	public void i_navigate_to_new_customer_page() throws Exception {
		element(ByUtils.getBy("newcustomer.menuitem")).click();
	}

	@When("^input new customer information$")
	public void input_new_customer_information(DataTable data) throws Exception {
		inputCustomerInformation(data.asMap(String.class, String.class));
	}

	@Then("^verify new customer is created$")
	public void verify_new_customer_is_created() throws Exception {
		Varriable.CustomerId = element(ByUtils.getBy("newcustomer.customerId")).getText();
		Assert.assertNotNull(Varriable.CustomerId);
		// recovery environment
		removeCustomer();
		closeDriver();

	}

	private void inputCustomerInformation(Map<String, String> data) {
		element(ByUtils.getBy("newcustomer.name")).sendKeys(data.get("name"));
		String genderPath = "newcustomer.gender." + data.get("gender") + ".select";
		element(ByUtils.getBy(genderPath)).click();
		element(ByUtils.getBy("newcustomer.dayofbirth")).sendKeys(data.get("dayofbirth"));
		element(ByUtils.getBy("newcustomer.address")).sendKeys(data.get("address"));
		element(ByUtils.getBy("newcustomer.city")).sendKeys(data.get("city"));
		element(ByUtils.getBy("newcustomer.state")).sendKeys(data.get("state"));
		element(ByUtils.getBy("newcustomer.pin")).sendKeys(data.get("pin"));
		element(ByUtils.getBy("newcustomer.phone")).sendKeys(data.get("phone"));
		element(ByUtils.getBy("newcustomer.email")).sendKeys(data.get("email"));
		element(ByUtils.getBy("newcustomer.password")).sendKeys(data.get("password"));
		element(ByUtils.getBy("newcustomer.submit.button")).click();

	}

	private void removeCustomer() {
		DeleteCustomer deleteCustomer = new DeleteCustomer();
		deleteCustomer.navigateToDelete();
		deleteCustomer.deleteCustomer();
	}
}
