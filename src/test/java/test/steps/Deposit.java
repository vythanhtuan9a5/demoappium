package test.steps;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test.utils.ByUtils;
import test.utils.Varriable;

public class Deposit extends BasePageAndroid {

	@When("^i navigate to Edit Account as \"([^\"]*)\" page to get account amount$")
	public void i_navigate_to_Edit_Account_as_page_to_get_account_amount(String accountId) throws Exception {
		element(ByUtils.getBy("editAccount.menuitem")).click();
		element(ByUtils.getBy("editAccount.account")).sendKeys(accountId);
		element(ByUtils.getBy("editAccount.submitbutton")).click();
		element(ByUtils.getBy("editAccount.submitbutton")).click();
		Varriable.AccountBalance = Integer
				.parseInt(element(ByUtils.getBy("editAccount.balance")).getAttribute("value"));

	}

	@When("^i navigate to Depopsit page$")
	public void i_navigate_to_Depopsit_page() throws Exception {
		element(ByUtils.getBy("deposit.menuitem")).click();

	}

	@When("^input User Account as \"([^\"]*)\" with amount as \"([^\"]*)\" and desciption as \"([^\\\"]*)\"$")
	public void input_User_Account_as_with_amount_as(String accountId, String amount, String description)
			throws Exception {
		Varriable.AccountBalance += Integer.parseInt(amount);
		element(ByUtils.getBy("deposit.accountid")).sendKeys(accountId);
		element(ByUtils.getBy("deposit.amount")).sendKeys(amount);
		element(ByUtils.getBy("deposit.description")).sendKeys(description);
		element(ByUtils.getBy("deposit.submitbutton")).click();

	}

	@Then("^verify deposit work correctly$")
	public void verify_deposit_work_correctly() throws Exception {
		Assert.assertEquals(element(ByUtils.getBy("deposit.currentbalance")).getText(), Varriable.AccountBalance + "");
		closeDriver();
	}
}
