package test.steps;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.gson.Gson;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BasePageAndroid {
	private static WebDriver driver;

	public BasePageAndroid() {
		if (driver == null) {
			System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
			BufferedReader br= null;
			try {
				br = new BufferedReader(new FileReader("cap.json"));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Gson gson = new Gson();
			DesiredCapabilities cap = gson.fromJson(br, DesiredCapabilities.class);
			System.out.println(cap);
			try {
				driver = new AndroidDriver<>(new URL(System.getProperty("saucelab.url")),cap);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			driver = new AndroidDriver<>()
		}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	public WebDriver getDriver() {
		return driver;
	}

	public WebElement element(By by) {
		return driver.findElement(by);
	}
	public void waitSeconds(int time) {
		try {
			Thread.sleep(time*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void closeDriver() {
		driver.close();
		driver =null;
	}
}
