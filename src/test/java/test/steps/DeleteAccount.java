package test.steps;

import test.utils.ByUtils;
import test.utils.Varriable;

public class DeleteAccount extends BasePageAndroid {
	public void navigateToDeleteAccount() {
		element(ByUtils.getBy("deleteAccount.menuitem")).click();
		
	}
	public void deleteAccount() {
		element(ByUtils.getBy("deleteAccount.accountNo")).sendKeys(Varriable.AccountId);
		element(ByUtils.getBy("deleteAccount.submitbutton")).click();
		waitSeconds(1);
		getDriver().switchTo().alert().accept();
	}
}
