package test.steps;

import test.utils.ByUtils;
import test.utils.Varriable;

public class DeleteCustomer extends BasePageAndroid{
	public void navigateToDelete() {
		element(ByUtils.getBy("deletecustomer.menuitem")).click();
		
	}
	public void deleteCustomer() {
		element(ByUtils.getBy("deletecustomer.inputcusID")).sendKeys(Varriable.CustomerId);
		element(ByUtils.getBy("deletecustomer.submitbutton")).click();
		waitSeconds(1);
		getDriver().switchTo().alert().accept();
		waitSeconds(2);
		getDriver().switchTo().alert().accept();
	}
}
