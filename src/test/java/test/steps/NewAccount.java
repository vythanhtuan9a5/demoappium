package test.steps;

import java.util.Map;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test.utils.ByUtils;
import test.utils.Varriable;
public class NewAccount extends BasePageAndroid {

@When("^i navigate to new Account page$")
public void i_navigate_to_new_Account_page() throws Exception {
	element(ByUtils.getBy("newAccount.menuitem")).click();
}

@When("^input new Account information$")
public void input_new_Account_information(DataTable arg1) throws Exception {
	Map<String, String> data = arg1.asMap(String.class, String.class);
	
	element(ByUtils.getBy("newAccount.CustomerId")).sendKeys(data.get("userId"));
	element(ByUtils.getBy("newAccount.AccountType")).sendKeys(data.get("type"));
	element(ByUtils.getBy("newAccount.InitialDeposit")).sendKeys(data.get("initial deposit"));
	element(ByUtils.getBy("newAccount.SubmitButton")).click();
}

@Then("^verify new Account is created$")
public void verify_new_Account_is_created() throws Exception {
	Varriable.AccountId = element(ByUtils.getBy("newAccount.AccountId")).getText();
	//recovery environment
	removeAccount();
	closeDriver();
}
private void removeAccount() {
	DeleteAccount deleteCustomer = new DeleteAccount();
	deleteCustomer.navigateToDeleteAccount();
	deleteCustomer.deleteAccount();
}
}
