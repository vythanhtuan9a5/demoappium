package test.steps;

import cucumber.api.java.en.Given;
import test.utils.ByUtils;

public class LoginPage extends BasePageAndroid{
	
	@Given("^me navigate to \"([^\"]*)\"$")
	public void me_navigate_to(String url) throws Exception {
		getDriver().get(url);
	}

	@Given("^username as \"([^\"]*)\" and password as \"([^\"]*)\" to login System$")
	public void username_as_and_password_as_to_login_System(String username, String password) throws Exception {
		element(ByUtils.getBy("userid.input")).sendKeys(username);
		element(ByUtils.getBy("password.input")).sendKeys(password);
		waitSeconds(2);
		element(ByUtils.getBy("login.button")).submit();
	}
}
