package test.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesUtils {
	public static Properties properties = new Properties(); 
	public static final String XPATH="object_repositories/XPath.properties";
	public static Properties getProperties(String path) {
		try {
			properties.load(new FileInputStream(path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;
	}
	public static Properties getObjectProperties() {
		return getProperties(XPATH);
	}
}
