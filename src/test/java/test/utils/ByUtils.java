package test.utils;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;

public class ByUtils {
	public static By getBy(String path) {
		Set<String> key = PropertiesUtils.getObjectProperties().stringPropertyNames();
		Iterator<String> iterKey = key.iterator();
		String fullPath = null;
		while (iterKey.hasNext()) {
			fullPath = iterKey.next();
			if (fullPath.toLowerCase().contains(path.toLowerCase())) {
				break;
			}
		}
		String type = fullPath.replace(path + ".", "");
		return createBy(type, PropertiesUtils.properties.getProperty(fullPath));

	}

	public static By createBy(String type, String value) {
		type = type.toLowerCase();
		switch (type) {
		case "xpath":
			return By.xpath(value);
		case "id":
			return By.id(value);
		case "name":
			return By.name(value);
		default:
			return null;
		}
	}
}
