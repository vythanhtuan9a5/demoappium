Feature: new customer feature

Scenario: create new customer
	Given me navigate to "http://demo.guru99.com/v4/"
	Given username as "mngr168110" and password as "qYmYruz" to login System
	When i navigate to new customer page
	And input new customer information
	|name|nguyen van a|
	|gender|male|
	|dayofbirth|02/22/2012|
	|address|nguyen van a ho chi minh city|
	|city|ho chi minh|
	|state|Viet Nam|
	|pin|123456|
	|phone|0123456789|
	|email|asdmobile@email.com|
	|password|123456|
	Then verify new customer is created